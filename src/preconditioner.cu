//
// Created by marshall on 5/2/19.
//

#include "preconditioner.h"
#include "edge.h"
#include "mesh_reader.h"
#include "interior_inverse.h"
#include "matrix_multiply.h"
#include <cmath>

__global__ void daxpy(double *a, const double *b, const int alpha, const int beta, const int size) {
    /*
     * Performs a = alpha*a + beta * b
     */
    for (int i = threadIdx.x + blockIdx.x * blockDim.x; i < size; i += blockDim.x * gridDim.x) {
        a[i] = alpha * a[i] + beta * b[i];
    }
}

__global__ void zero(double *a, const int size) {
    /*
     * Zeros out the vector
     */
    for (int i = threadIdx.x + blockIdx.x * blockDim.x; i < size; i += blockDim.x * gridDim.x) {
        a[i] = 0.0;
    }
}

__global__ void scale_vertex(double *a, const double *b, const double *scales, const int p, const int size) {
    /*
     * Performs a = p^3 / (scales * b)
     */
    for (int i = threadIdx.x + blockIdx.x * blockDim.x; i < size; i += blockDim.x * gridDim.x) {
        a[i] = pow(p, 3) / scales[i] * b[i];
    }
}

void precondition(const double *input, double *output, double *temp_rhs, double *temp_rhs2, mesh *mymesh,
                  interior_infos *int_infos, edge_info *e_info) {

    const int int_dofs = int((mymesh->p - 1) * (mymesh->p - 2) / 2);
    const int boundary_length = mymesh->total_dofs - int_dofs * mymesh->num_elements;

    // Zero out vector
//    cudaMemset(temp_rhs, 0, mymesh->total_dofs * sizeof(double));
    zero<<<int((mymesh->total_dofs) / 32) + 1, 32>>>(temp_rhs, mymesh->total_dofs);
    process_interior << < mymesh->num_elements, 3*mymesh->q,
            sizeof(double) * int_infos->cuda_size >> >
            (&(input[boundary_length]), &(output[boundary_length]), int_infos);
    mass_multiply_b << < mymesh->num_elements, 2*mymesh->q,
            sizeof(double) * mymesh->cuda_size_mass >> >
            (output, temp_rhs, mymesh);
    daxpy << < int((mymesh->total_dofs) / 32) + 1, 32 >> > (temp_rhs, input, -1, 1, boundary_length);

    // Solve edges
    cudaDeviceSynchronize();
    process_edge << < e_info->edges, 2*mymesh->p,
            e_info->cuda_size * sizeof(double) >> >
            (&(temp_rhs[mymesh->num_vertices]), &(output[mymesh->num_vertices]), e_info);
    cudaDeviceSynchronize();
    // for (int i = 0; i < mymesh->total_dofs; ++i)
    // {
    //     if (isnan(output[i]))
    //     {
    //         printf("%d %g\n", i, output[i]);
    //     }
    // }

    // Solve vertex
    scale_vertex << < int((mymesh->total_dofs) / 32) + 1, 32 >> >
                                                          (output, temp_rhs, e_info->vertex_diags, mymesh->p, mymesh->num_vertices);
    // Now backsolve after
    zero<<<int((mymesh->total_dofs) / 32) + 1, 32>>>(temp_rhs, mymesh->total_dofs);
    mass_multiply_bt << < mymesh->num_elements, 2*mymesh->q,
            sizeof(double) * mymesh->cuda_size_mass >> >
            (output, temp_rhs, mymesh);

    zero<<<int((mymesh->total_dofs) / 32) + 1, 32>>>(temp_rhs2, mymesh->total_dofs);
    process_interior << < mymesh->num_elements, 2*mymesh->q,
            sizeof(double) * int_infos->cuda_size >> >
            (temp_rhs, &(temp_rhs2[boundary_length]), int_infos);

    daxpy <<< int((mymesh->total_dofs) / 32) + 1, 32 >> > (output, temp_rhs2, 1, -1, mymesh->total_dofs);
//    cudaDeviceSynchronize();
}