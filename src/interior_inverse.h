//
// Created by marshall on 5/2/19.
//

#ifndef BERNSTEIN_CUDA_INTERIOR_INVERSE_H
#define BERNSTEIN_CUDA_INTERIOR_INVERSE_H
struct interior_infos {
    int num_internal_dofs;
    double* scaling;
//    ArrayXXd j2b;
    double** q;
    int num_elements;
    int p;
    int cuda_size;
};

__global__ void process_interior(const double *x, double *out, interior_infos *int_infos);
#endif //BERNSTEIN_CUDA_INTERIOR_INVERSE_H
