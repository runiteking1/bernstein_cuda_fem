//
// Created by marshall on 4/15/19.
//

#ifndef BERNSTEINCPP_READ_MSH_H
#define BERNSTEINCPP_READ_MSH_H

#include <cstdio>
#include <string>
#include <map>
#include <vector>

// Very simple mesh structure; we are not dealing with Dirichlet boundary conditions
struct mesh {
    double* vertices;
    int* elnode;
    int num_elements;
    int num_vertices;
    int p; // Order
    int q; // Quadrature rule
    int total_dofs; // Total numebr of dofs
    int dofs_per_element; // number of dofs per element
    int* eldof;
    double *areas;
    int num_edges;

    int** lookup_dof;
    int** lookup_multi;
    double **xis;
    double **omegas;

    int cuda_size_mass;
    int cuda_size_stiff;
};

void read_msh_file(std::string gmsh_filename, mesh* mymesh, bool verbose = false);
void construct_elnode(mesh *mymesh, bool verbose = true);

#endif //BERNSTEINCPP_READ_MSH_H
