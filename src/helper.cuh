//
// Created by marshall on 5/1/19.
//

#ifndef BERNSTEIN_CUDA_HELPER_CUH
#define BERNSTEIN_CUDA_HELPER_CUH
double** mem_alloc2D_double(unsigned N_rows, unsigned N_cols);
int** mem_alloc2D_int(unsigned N_rows, unsigned N_cols);
#endif //BERNSTEIN_CUDA_HELPER_CUH
