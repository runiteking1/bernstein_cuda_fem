//
// Created by marshall on 5/3/19.
//

#ifndef BERNSTEIN_CUDA_PCG_H
#define BERNSTEIN_CUDA_PCG_H

#include <cublas_v2.h>
#include "mesh_reader.h"
#include "interior_inverse.h"
#include "edge.h"
#include "preconditioner.h"

struct pcg_variables {
    cublasHandle_t handle;
    double* g;
    double* h;
    double* d;
    double* precond_1;
    double* precond_2;
};

void set_pcg_struct(pcg_variables *pcgVariables, int dof_size);
void mass_pcg_precond(const double *b, double *x, mesh *mymesh, interior_infos *int_infos,
                      edge_info *e_info, pcg_variables *pcgVariables);
#endif //BERNSTEIN_CUDA_PCG_H
