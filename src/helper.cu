//
// Created by marshall on 5/1/19.
//

#include "helper.cuh"
#include <cstdio>
#include "cudaHeaders.h"

double** mem_alloc2D_double(unsigned N_rows, unsigned N_cols) {
    double **U;
    cudaMallocManaged(&U, N_rows*sizeof(double*));
    cudaMallocManaged(&U[0],N_rows*N_cols*sizeof(double));
    for (unsigned i = 1; i < N_rows; ++i) {
        U[i] = U[0] + i*N_cols;
    }
    return U;
}

int **mem_alloc2D_int(unsigned N_rows, unsigned N_cols) {
    int **U;
    cudaMallocManaged(&U, N_rows*sizeof(int*));
    cudaMallocManaged(&U[0],N_rows*N_cols*sizeof(int));
    for (unsigned i = 1; i < N_rows; ++i) {
        U[i] = U[0] + i*N_cols;
    }
    return U;
}



void print_matrix(double **A, int n, int m) {
    printf("Printing Matrix :\n");
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++)
            printf("%g ", A[i][j]);
        printf("\n");
    }
    printf("\n");
}

void delete_mem2d(double **arr) {
    delete[] arr[0];  // remove the pool
    delete[] arr;     // remove the pointers
}
