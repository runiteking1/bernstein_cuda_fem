//
// Created by marshall on 5/1/19.
//

#ifndef BERNSTEIN_CUDA_MATRIX_MULTIPLY_H
#define BERNSTEIN_CUDA_MATRIX_MULTIPLY_H
#include "mesh_reader.h"
#include "cudaHeaders.h"
__device__ double rhs(const double x, const double y);
__global__ void mass_multiply(const double *x, double *Ax, const mesh *mymesh);
__global__ void load_vector(double *return_vector, const mesh *mymesh);

__global__ void mass_multiply_b(const double *x, double *Ax, const mesh *mymesh);

__global__ void mass_multiply_bt(const double *x, double *Ax, const mesh *mymesh);
__global__ void stiff_multiply(const double *x, double *Ax, const mesh *mymesh);
__global__ void nonlinear_sine(const double *x, double *sinx, const mesh *mymesh);
#endif //BERNSTEIN_CUDA_MATRIX_MULTIPLY_H
