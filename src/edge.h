//
// Created by marshall on 5/2/19.
//

#ifndef BERNSTEIN_CUDA_EDGE_H
#define BERNSTEIN_CUDA_EDGE_H

#include "mesh_reader.h"

struct edge_info {
    double *edge_diags; // Create an array corresponding to each dof on the edges
    double *vertex_diags;             // and for each vertex
    double **algo_coefs;        // Coefficients for edge transformations
    double **cinv_coefs;
    int edges;
    int edge_dofs;
    int cuda_size;
};

void prepare_preconds(mesh *mymesh, edge_info *e_info);

__global__ void process_edge(double *x, double *output, edge_info *e_info);

#endif //BERNSTEIN_CUDA_EDGE_H
