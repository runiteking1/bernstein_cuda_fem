//
// Created by marshall on 5/1/19.
//

#include "matrix_multiply.h"
#include "mesh_reader.h"
#include <iostream>
//#include <cublas_v2.h>
#include "cudaHeaders.h"
#include <helper.cuh>

__device__ double rhs(const double x, const double y) {
//    return x + y;
    return 4 * atan(exp(x + 1 - 2/cosh(y + 7) - 2/cosh(y - 7)));
//    return pow((-x - y) / 2, 2)*pow((x + 1) / 2, 1) * pow((y + 1) / 2, 1) * (y-1)/2 * (x-1)/2;
}

__device__ double inline atomicAdd_local(double *address, double val) {
    unsigned long long int *address_as_ull =
            (unsigned long long int *) address;
    unsigned long long int old = *address_as_ull, assumed;
    do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed,
                        __double_as_longlong(val +
                                             __longlong_as_double(assumed)));
    } while (assumed != old);
    return __longlong_as_double(old);
}

template<typename T>
__device__ void printArray(T *array, int N1, int N2) {
    if (threadIdx.x == 0 && threadIdx.y == 0) {
        for (int i = 0; i < N1; ++i) {
            for (int j = 0; j < N2; ++j) {
                printf("%g ", array[i + N1 * j]);
            }
            printf("\n");
        }
    }
}

__device__ void evaluate(double *c0, const int q, const int p, const mesh *mymesh, double *step1, double *step2) {
    int i_l, alphas, alpha_l;
    double xi, s, r, w;

    int l = 2;
    for (i_l = threadIdx.x; i_l < q; i_l += blockDim.x) {
        xi = mymesh->xis[2 - l][i_l];
        if (xi < .5) {
            s = 1 - xi;
            r = xi / s;

            for (alphas = 0; alphas < p + 1; alphas += 1) {
                w = pow(s, p - alphas);
                for (alpha_l = 0; alpha_l < p - alphas + 1; ++alpha_l) {
                    step1[alphas + (p + 1) * i_l] += w * c0[alphas + (p + 1) * alpha_l];

                    w *= r * (p - alphas - alpha_l) / (1.0 + alpha_l);
                }
            }
        }
        else {
            s = xi;
            r = (1-xi) / s;

            for (alphas = p; alphas > -1; alphas -= 1) {
                w = pow(s, p - alphas);
                for (alpha_l =  p - alphas ; alpha_l> -1; --alpha_l) {
                    step1[alphas + (p + 1) * i_l] += w * c0[alphas + (p + 1) * alpha_l];

                    w *= r * (alpha_l) / (1.0 + p - alphas - alpha_l);
                }

            }
        }
    }
    __syncthreads();

    l = 1;
    for (i_l = threadIdx.x; i_l < q; i_l += blockDim.x) {
        xi = mymesh->xis[2 - l][i_l];
        if (xi < .5) {
            s = 1 - xi;
            r = xi / s;

            w = pow(s, p);

            for (alpha_l = 0; alpha_l < p + 1; ++alpha_l) {
                for (int i_list = 0; i_list < q; i_list += 1) {
                    step2[i_l + q * i_list] += w * step1[alpha_l + (p + 1) * i_list];
                }

                w *= r * (p - alpha_l) / (1.0 + alpha_l);
            }
        }
        else {
            s = xi;
            r = (1-xi) / s;

            w = pow(s, p);

            for (alpha_l = p; alpha_l > -1; --alpha_l) {
                for (int i_list = q-1; i_list > -1; i_list -= 1) {
                    step2[i_l + q * i_list] += w * step1[alpha_l + (p + 1) * i_list];
                }

                w *= r * (alpha_l) / (1.0 + p - alpha_l );
            }

        }
    }
    __syncthreads();


}

__device__ void moment(double *fa, const int q, const int p, const mesh *mymesh, double *step_0, double *step_1) {
    int i_l, alpha_l;
    double xi, s, r, w, omega;

    int l = 1;
    for (i_l = threadIdx.x; i_l < q; i_l += blockDim.x) {
        xi = mymesh->xis[2 - l][i_l];
        if (xi < .5) {
            omega = mymesh->omegas[2 - l][i_l];

            s = 1 - xi;
            r = xi / s;
//            printf("hello from thread %d\n", threadIdx.x);
            w = omega * (pow(s, p));
            for (alpha_l = 0; alpha_l < p + 1; alpha_l += 1) {
                for (int i_list = 0; i_list < q; ++i_list) {
//                    step_0[alpha_l + (p + 1) * i_list] += w * fa[i_l + q * i_list];
                    atomicAdd_local(&(step_0[alpha_l + (p + 1) * i_list]), w * fa[i_l + q * i_list]);
                }

                w *= r * (p - alpha_l) / (1.0 + alpha_l);
            }
        }
        else {
            omega = mymesh->omegas[2 - l][i_l];

            s = xi;
            r = (1-xi) / s;
            w = omega * (pow(s, p));
            for (alpha_l = p; alpha_l > -1; alpha_l -= 1) {
                for (int i_list = q - 1; i_list > -1; --i_list) {
//                    step_0[alpha_l + (p + 1) * i_list] += w * fa[i_l + q * i_list];
                    atomicAdd_local(&(step_0[alpha_l + (p + 1) * i_list]), w * fa[i_l + q * i_list]);
                }

                w *= r * ( alpha_l) / (1.0 + p - alpha_l);
            }

        }
    }
    __syncthreads();

    l = 2;
    for (int i_l = threadIdx.x; i_l < q; i_l += blockDim.x) {
        xi = mymesh->xis[2 - l][i_l];

        if (xi < .5) {
            omega = mymesh->omegas[2 - l][i_l];

            s = 1 - xi;
            r = xi / s;

            for (int alphas = 0; alphas < p + 1; ++alphas) {
                w = omega * (pow(s, p - alphas));
                for (int alpha_l = 0; alpha_l < p - alphas + 1; ++alpha_l) {
//                step_1[alphas + (p + 1) * alpha_l] += w * step_0[alphas + (p + 1) * i_l];
                    atomicAdd_local(&(step_1[alphas + (p + 1) * alpha_l]), w * step_0[alphas + (p + 1) * i_l]);
                    w *= r * (p - alphas - alpha_l) / (1.0 + alpha_l);
                }
            }
        }
        else {
            omega = mymesh->omegas[2 - l][i_l];

            s = xi;
            r = (1-xi) / s;

            for (int alphas = p; alphas > -1; --alphas) {
                w = omega * (pow(s, p - alphas));
                for (int alpha_l = p - alphas; alpha_l > -1; --alpha_l) {
//                step_1[alphas + (p + 1) * alpha_l] += w * step_0[alphas + (p + 1) * i_l];
                    atomicAdd_local(&(step_1[alphas + (p + 1) * alpha_l]), w * step_0[alphas + (p + 1) * i_l]);
                    w *= r * (alpha_l) / (1.0 + p - alphas - alpha_l);
                }
            }

        }
    }

    __syncthreads();

}

__device__ void duffy(const double *t, const int element, const mesh *mymesh, double *result) {
    /*
     * takes in t a 2d variable and x which describes a simplex and outputs result a 2d array which is the result of the duffy transform
     */
    const double x1 = mymesh->vertices[2 * mymesh->elnode[element * 3 + 0]];
    const double y1 = mymesh->vertices[2 * mymesh->elnode[element * 3 + 0] + 1];
    const double x2 = mymesh->vertices[2 * mymesh->elnode[element * 3 + 1] + 0];
    const double y2 = mymesh->vertices[2 * mymesh->elnode[element * 3 + 1] + 1];
    const double x3 = mymesh->vertices[2 * mymesh->elnode[element * 3 + 2] + 0];
    const double y3 = mymesh->vertices[2 * mymesh->elnode[element * 3 + 2] + 1];

    result[0] = t[0] * x1 + (t[1] * (1 - t[0])) * x2 + (1 - t[0] - (t[1] * (1 - t[0]))) * x3;
    result[1] = t[0] * y1 + (t[1] * (1 - t[0])) * y2 + (1 - t[0] - (t[1] * (1 - t[0]))) * y3;
}

__device__ void element_load(double (*func)(double, double), const mesh *mymesh, const int element,
                             double *fa, double *step_0, double *step_1) {
    double t[2];
    double coord[2];

    for (int i = threadIdx.x; i < mymesh->q; i += blockDim.x) {
        for (int j = 0; j < mymesh->q; ++j) {
            t[0] = mymesh->xis[1][i];
            t[1] = mymesh->xis[0][j];
            duffy(t, element, mymesh, coord);
            fa[i + mymesh->q * j] = func(coord[0], coord[1]);
        }
    }

    moment(fa, mymesh->q, mymesh->p, mymesh, step_0, step_1);

    for (int i = threadIdx.x; i < mymesh->p + 1; i += blockDim.x) {
        for (int j = 0; j < mymesh->p + 1 - i; j += 1) {
            step_1[i + (mymesh->p + 1) * j] =
                    step_1[i + (mymesh->p + 1) * j] * 2 * mymesh->areas[element];
        }
    }
}

__global__ void load_vector(double *return_vector, const mesh *mymesh) {
    extern __shared__ double s[]; // Need a single shared extern for multiple shared memory
    double *fa_element = s;
    double *step_0 = (double *) &fa_element[(mymesh->q) * (mymesh->q)];
    double *step_1 = (double *) &step_0[(mymesh->p + 1) * (mymesh->q)];

    // First inititalize shared memory to 0
    for (int i = threadIdx.x;
         i < ((mymesh->p + 1) * (mymesh->p + 1) + (mymesh->p + 1) * (mymesh->q) +
              (mymesh->q * mymesh->q)); i += blockDim.x) {
        s[i] = 0;
    }

    __syncthreads();
    for (int element = blockIdx.x; element < mymesh->num_elements; element += gridDim.x) {
        element_load(rhs, mymesh, element, fa_element, step_0, step_1);
        __syncthreads();
        for (int i = threadIdx.x; i < mymesh->dofs_per_element; i += blockDim.x) {
            atomicAdd_local(&(return_vector[mymesh->eldof[element * mymesh->dofs_per_element + i]]),
                            step_1[mymesh->lookup_dof[i][0] +
                                   (mymesh->p + 1) * mymesh->lookup_dof[i][1]]);
        }

    }
}

__global__
void mass_multiply(const double *x, double *Ax, const mesh *mymesh) {
    extern __shared__ double s[]; // Need a single shared extern for multiple shared memory
    double *ca_element = s;
    double *step_0 = (double *) &ca_element[(mymesh->p + 1) * (mymesh->p + 1)];
    double *step_1 = (double *) &step_0[(mymesh->p + 1) * (mymesh->q)];

    // First inititalize shared memory to 0
    for (int i = threadIdx.x;
         i < ((mymesh->p + 1) * (mymesh->p + 1) + (mymesh->p + 1) * (mymesh->q) +
              (mymesh->q * mymesh->q)); i += blockDim.x) {
        s[i] = 0;
    }

    for (int element = blockIdx.x; element < mymesh->num_elements; element += gridDim.x) {
        // Need to put vector into a form that I can use (assumes block size if of p + 1 greater each direction
        for (int i = threadIdx.x; i < mymesh->p + 1; i += blockDim.x) {
            for (int j = 0; j < mymesh->p + 1 - i; j += 1) {
                ca_element[i + (mymesh->p + 1) * j] =
                        x[mymesh->eldof[element * mymesh->dofs_per_element + mymesh->lookup_multi[i][j]]];
            }
        }

        evaluate(ca_element, mymesh->q, mymesh->p, mymesh, step_0, step_1);

        // Zero out ca_element and step_0
        for (int i = threadIdx.x;
             i < (mymesh->p + 1) * (mymesh->p + 1) + (mymesh->p + 1) * mymesh->q; i += blockDim.x) {
            s[i] = 0;
        }
        __syncthreads();
        moment(step_1, mymesh->q, mymesh->p, mymesh, step_0, ca_element);

        for (int i = threadIdx.x; i < mymesh->p + 1; i += blockDim.x) {
            for (int j = 0; j < mymesh->p + 1 - i; j += 1) {
                ca_element[i + (mymesh->p + 1) * j] =
                        ca_element[i + (mymesh->p + 1) * j] * 2 * mymesh->areas[element];
            }
        }

        __syncthreads();
        for (int i = threadIdx.x + (mymesh->dofs_per_element - (mymesh->p - 2) * (mymesh->p - 1) / 2);
             i < mymesh->dofs_per_element; i += blockDim.x) { // Update interior dofs first, no atomics
            Ax[mymesh->eldof[element * mymesh->dofs_per_element + i]] +=
                    ca_element[mymesh->lookup_dof[i][0] +
                               (mymesh->p + 1) * mymesh->lookup_dof[i][1]];
        }
        for (int i = threadIdx.x; i < mymesh->dofs_per_element - (mymesh->p - 2) * (mymesh->p - 1) / 2; i += blockDim.x) {
            atomicAdd_local(&(Ax[mymesh->eldof[element * mymesh->dofs_per_element + i]]),
                            ca_element[mymesh->lookup_dof[i][0] +
                                       (mymesh->p + 1) * mymesh->lookup_dof[i][1]]);
        }
    }
}

__global__
void mass_multiply_b(const double *x, double *Ax, const mesh *mymesh) {
    extern __shared__ double s[]; // Need a single shared extern for multiple shared memory
    double *ca_element = s;
    double *step_0 = (double *) &ca_element[(mymesh->p + 1) * (mymesh->p + 1)];
    double *step_1 = (double *) &step_0[(mymesh->p + 1) * (mymesh->q)];

    // First inititalize shared memory to 0
    for (int i = threadIdx.x;
         i < ((mymesh->p + 1) * (mymesh->p + 1) + (mymesh->p + 1) * (mymesh->q) +
              (mymesh->q * mymesh->q)); i += blockDim.x) {
        s[i] = 0;
    }
    __syncthreads();


    for (int element = blockIdx.x; element < mymesh->num_elements; element += gridDim.x) {
        // Need to put vector into a form that I can use (assumes block size if of p + 1 greater each direction
        for (int i = threadIdx.x + 1; i < mymesh->p - 1; i += blockDim.x) {
            for (int j = 1; j < mymesh->p - i; j += 1) {
                ca_element[i + (mymesh->p + 1) * j] =
                        x[mymesh->eldof[element * mymesh->dofs_per_element + mymesh->lookup_multi[i][j]]];
            }
        }

        evaluate(ca_element, mymesh->q, mymesh->p, mymesh, step_0, step_1);

        // Zero out ca_element and step_0
        for (int i = threadIdx.x;
             i < (mymesh->p + 1) * (mymesh->p + 1) + (mymesh->p + 1) * mymesh->q; i += blockDim.x) {
            s[i] = 0;
        }
        __syncthreads();
        moment(step_1, mymesh->q, mymesh->p, mymesh, step_0, ca_element);

        for (int i = threadIdx.x; i < mymesh->p + 1; i += blockDim.x) {
            for (int j = 0; j < mymesh->p + 1 - i; j += 1) {
                ca_element[i + (mymesh->p + 1) * j] =
                        ca_element[i + (mymesh->p + 1) * j] * 2 * mymesh->areas[element];
            }
        }

        __syncthreads();
        for (int i = threadIdx.x;
             i < mymesh->dofs_per_element - (mymesh->p - 2) * (mymesh->p - 1) / 2; i += blockDim.x) {
            atomicAdd_local(&(Ax[mymesh->eldof[element * mymesh->dofs_per_element + i]]),
                            ca_element[mymesh->lookup_dof[i][0] +
                                       (mymesh->p + 1) * mymesh->lookup_dof[i][1]]);
        }

    }

}

__global__
void mass_multiply_bt(const double *x, double *Ax, const mesh *mymesh) {
    extern __shared__ double s[]; // Need a single shared extern for multiple shared memory
    double *ca_element = s;
    double *step_0 = (double *) &ca_element[(mymesh->p + 1) * (mymesh->p + 1)];
    double *step_1 = (double *) &step_0[(mymesh->p + 1) * (mymesh->q)];

    // First inititalize shared memory to 0
    for (int i = threadIdx.x;
         i < ((mymesh->p + 1) * (mymesh->p + 1) + (mymesh->p + 1) * (mymesh->q) +
              (mymesh->q * mymesh->q)); i += blockDim.x) {
        s[i] = 0;
    }
    __syncthreads();

    for (int element = blockIdx.x; element < mymesh->num_elements; element += gridDim.x) {
        // Need to put vector into a form that I can use (assumes block size if of p + 1 greater each direction
        for (int i = threadIdx.x; i < mymesh->p + 1; i += blockDim.x) {
            for (int j = 0; j < mymesh->p + 1 - i; j += 1) {
                if (i == 0 || j == 0 || mymesh->p - i - j == 0) {
                    ca_element[i + (mymesh->p + 1) * j] =
                            x[mymesh->eldof[element * mymesh->dofs_per_element + mymesh->lookup_multi[i][j]]];
                }
            }
        }

        evaluate(ca_element, mymesh->q, mymesh->p, mymesh, step_0, step_1);

        // Zero out ca_element and step_0
        for (int i = threadIdx.x;
             i < (mymesh->p + 1) * (mymesh->p + 1) + (mymesh->p + 1) * mymesh->q; i += blockDim.x) {
            s[i] = 0;
        }
        __syncthreads();
        moment(step_1, mymesh->q, mymesh->p, mymesh, step_0, ca_element);

        for (int i = threadIdx.x; i < mymesh->p + 1; i += blockDim.x) {
            for (int j = 0; j < mymesh->p + 1 - i; j += 1) {
                ca_element[i + (mymesh->p + 1) * j] =
                        ca_element[i + (mymesh->p + 1) * j] * 2 * mymesh->areas[element];
            }
        }
        __syncthreads();

        // Only update interior dofs
        for (int i = threadIdx.x + mymesh->dofs_per_element - (mymesh->p - 2) * (mymesh->p - 1) / 2;
             i < mymesh->dofs_per_element; i += blockDim.x) {
            // interior dofs don't need atomic
//            atomicAdd_local(&(Ax[mymesh->eldof[element * mymesh->dofs_per_element + i] - mymesh->num_vertices -
//                                 (mymesh->p - 1) * mymesh->num_edges]),
//                            ca_element[mymesh->lookup_dof[i][0] +
//                                       (mymesh->p + 1) * mymesh->lookup_dof[i][1]]);
            Ax[mymesh->eldof[element * mymesh->dofs_per_element + i] - mymesh->num_vertices -
                                 (mymesh->p - 1) * mymesh->num_edges] +=
                            ca_element[mymesh->lookup_dof[i][0] +
                                       (mymesh->p + 1) * mymesh->lookup_dof[i][1]];

        }

    }

//    if (threadIdx.x == 0 and threadIdx.y == 0 and blockIdx.x == 0) {
//        printf("Done with matrix mult\n");
//    }

}

__device__ double norm2d(double *vec) {
    return sqrt(vec[0] * vec[0] + vec[1] * vec[1]);
}

__global__
void stiff_multiply(const double *x, double *Ax, const mesh *mymesh) {
    extern __shared__ double s[]; // Need a single shared extern for multiple shared memory
    double *ca_element = s;
    double *ca_element_x = (double *) &ca_element[(mymesh->p + 1) * (mymesh->p + 1)];
    double *ca_element_y = (double *) &ca_element_x[(mymesh->p) * (mymesh->p)];

    double *step_0_x = (double *) &ca_element_y[(mymesh->p) * (mymesh->p)];
    double *step_0_y = (double *) &step_0_x[(mymesh->p + 1) * (mymesh->q)];
    double *step_1_x = (double *) &step_0_y[(mymesh->p + 1) * (mymesh->q)];
    double *step_1_y = (double *) &step_1_x[(mymesh->q) * (mymesh->q)];

    double *output_element = (double *) &step_1_y[(mymesh->q) * (mymesh->q)];

    double *v0 = (double *) &output_element[(mymesh->p + 1) * (mymesh->p + 1)];
    double *v1 = (double *) &v0[2];
    double *v2 = (double *) &v1[2];
    double *t0 = (double *) &v2[2];
    double *t1 = (double *) &t0[2];
    double *t2 = (double *) &t1[2];
    double *n0 = (double *) &t2[2];
    double *n1 = (double *) &n0[2];
    double *n2 = (double *) &n1[2];


    // First inititalize shared memory to 0
    for (int i = threadIdx.x;
         i < (2*(mymesh->p + 1) * (mymesh->p + 1) + 2*(mymesh->p) * (mymesh->p)  + 2*(mymesh->p + 1) * (mymesh->q) +
              2*(mymesh->q * mymesh->q) + 18); i += blockDim.x) {
        s[i] = 0;
    }
    __syncthreads();

    for (int element = blockIdx.x; element < mymesh->num_elements; element += gridDim.x) {
        // Need to put vector into a (vector) form that I can use (assumes block size if of p + 1 greater each direction
        for (int i = threadIdx.x; i < mymesh->p + 1; i += blockDim.x) {
            for (int j = 0; j < mymesh->p + 1 - i; j += 1) {
                ca_element[i + (mymesh->p + 1) * j] =
                        x[mymesh->eldof[element * mymesh->dofs_per_element + mymesh->lookup_multi[i][j]]];
            }
        }

        // Find the tangents and normals
        if (threadIdx.x == 0) {
            v0[0] = mymesh->vertices[2 * mymesh->elnode[3 * element]];
            v0[1] = mymesh->vertices[2 * mymesh->elnode[3 * element] + 1];
        }
        else if (threadIdx.x == 1) {
            v1[0] = mymesh->vertices[2 * mymesh->elnode[3 * element + 1]];
            v1[1] = mymesh->vertices[2 * mymesh->elnode[3 * element + 1] + 1];
        }
        else if (threadIdx.x == 2) {
            v2[0] = mymesh->vertices[2 * mymesh->elnode[3 * element + 2]];
            v2[1] = mymesh->vertices[2 * mymesh->elnode[3 * element + 2] + 1];
        }

        __syncthreads();
        if (threadIdx.x == 0) {
            t0[0] = -(v0[0] - v1[0]);
            t0[1] = -(v0[1] - v1[1]);
            n0[0] = t0[1] / norm2d(t0);
            n0[1] = -t0[0] / norm2d(t0);

        }
        else if (threadIdx.x == 1) {
            t1[0] = -(v1[0] - v2[0]);
            t1[1] = -(v1[1] - v2[1]);
            n1[0] = t1[1] / norm2d(t1);
            n1[1] = -t1[0] / norm2d(t1);

        }
        else if (threadIdx.x == 2) {
            t2[0] = -(v2[0] - v0[0]);
            t2[1] = -(v2[1] - v0[1]);
            n2[0] = t2[1] / norm2d(t2);
            n2[1] = -t2[0] / norm2d(t2);

        }

        __syncthreads();
        for (int alpha1 = threadIdx.x; alpha1 < mymesh->p; alpha1 += blockDim.x) {
            for (int alpha2 = 0; alpha2 < mymesh->p - alpha1; ++alpha2) {
                ca_element_x[alpha1 + mymesh->p * alpha2] =
                        -mymesh->p * (ca_element[alpha1 + (mymesh->p + 1) * alpha2] * n0[0] * norm2d(t0) +
                                      ca_element[alpha1 + 1 + (mymesh->p + 1) * alpha2] * n1[0] * norm2d(t1) +
                                      ca_element[alpha1 + (mymesh->p + 1) * (alpha2 + 1)] * n2[0] * norm2d(t2)) /
                        (2 * mymesh->areas[element]);
                ca_element_y[alpha1 + mymesh->p * alpha2] =
                        -mymesh->p * (ca_element[alpha1 + (mymesh->p + 1) * alpha2] * n0[1] * norm2d(t0) +
                                      ca_element[alpha1 + 1 + (mymesh->p + 1) * alpha2] * n1[1] * norm2d(t1) +
                                      ca_element[alpha1 + (mymesh->p + 1) * (alpha2 + 1)] * n2[1] * norm2d(t2)) /
                        (2 * mymesh->areas[element]);
            }
        }

        evaluate(ca_element_x, mymesh->q, mymesh->p - 1, mymesh, step_0_x, step_1_x);
        evaluate(ca_element_y, mymesh->q, mymesh->p - 1, mymesh, step_0_y, step_1_y);

        // Reuse v's
        if (threadIdx.x == 0) {
            v0[0] = -norm2d(t0) * n0[0] / (2 * mymesh->areas[element]);
            v0[1] = -norm2d(t0) * n0[1] / (2 * mymesh->areas[element]);
        }
        else if (threadIdx.x == 1) {
            v1[0] = -norm2d(t1) * n1[0] / (2 * mymesh->areas[element]);
            v1[1] = -norm2d(t1) * n1[1] / (2 * mymesh->areas[element]);
        }
        else if (threadIdx.x == 2) {
            v2[0] = -norm2d(t2) * n2[0]/ (2 * mymesh->areas[element]);
            v2[1] = -norm2d(t2) * n2[1]/ (2 * mymesh->areas[element]);
        }


        // Zero out ca_element and step_0 // TODO: need to make sure
        for (int i = threadIdx.x;
             i < (mymesh->p + 1) * (mymesh->p + 1) + 2*(mymesh->p) * (mymesh->p) + 2*(mymesh->p + 1) * mymesh->q; i += blockDim.x) {
            s[i] = 0;
        }
        __syncthreads();

        moment(step_1_x, mymesh->q, mymesh->p - 1, mymesh, step_0_x, ca_element_x);

        moment(step_1_y, mymesh->q, mymesh->p - 1, mymesh, step_0_y, ca_element_y);


        for (int i = threadIdx.x; i < mymesh->p; i += blockDim.x) {
            for (int j = 0; j < mymesh->p - i; j += 1) {
                ca_element_x[i + (mymesh->p) * j] =
                        ca_element_x[i + (mymesh->p) * j] * 2 * mymesh->areas[element];
                ca_element_y[i + (mymesh->p) * j] =
                        ca_element_y[i + (mymesh->p) * j] * 2 * mymesh->areas[element];
            }
        }
        __syncthreads();

        for (int alpha1 = threadIdx.x; alpha1 < mymesh->p; alpha1 += blockDim.x) {
            for (int alpha2 = 0; alpha2 < mymesh->p - alpha1; ++alpha2) {
                output_element[alpha1 + 1 + (mymesh->p + 1)*alpha2] +=
                        mymesh->p * (ca_element_x[alpha1 + mymesh->p * alpha2] * v1[0] + ca_element_y[alpha1 + mymesh->p * alpha2] * v1[1]);
                output_element[alpha1 + (mymesh->p + 1)*(alpha2 + 1)] +=
                        mymesh->p * (ca_element_x[alpha1 + mymesh->p*alpha2] * v2[0] + ca_element_y[alpha1 + mymesh->p * alpha2] * v2[1]);
                output_element[alpha1 + (mymesh->p + 1)*alpha2] +=
                        mymesh->p * (ca_element_x[alpha1 + mymesh->p*alpha2] * v0[0] + ca_element_y[alpha1 + mymesh->p * alpha2] * v0[1]);
            }
        }

        __syncthreads();
        for (int i = threadIdx.x; i < mymesh->dofs_per_element; i += blockDim.x) {
            atomicAdd_local(&(Ax[mymesh->eldof[element * mymesh->dofs_per_element + i]]),
                            output_element[mymesh->lookup_dof[i][0] +
                                       (mymesh->p + 1) * mymesh->lookup_dof[i][1]]);
        }
    }

}

__global__
void nonlinear_sine(const double *x, double *sinx, const mesh *mymesh) {
    extern __shared__ double s[]; // Need a single shared extern for multiple shared memory
    double *ca_element = s;
    double *step_0 = (double *) &ca_element[(mymesh->p + 1) * (mymesh->p + 1)];
    double *step_1 = (double *) &step_0[(mymesh->p + 1) * (mymesh->q)];

    // First inititalize shared memory to 0
    for (int i = threadIdx.x;
         i < ((mymesh->p + 1) * (mymesh->p + 1) + (mymesh->p + 1) * (mymesh->q) +
              (mymesh->q * mymesh->q)); i += blockDim.x) {
        s[i] = 0;
    }
    __syncthreads();

    for (int element = blockIdx.x; element < mymesh->num_elements; element += gridDim.x) {
        // Need to put vector into a form that I can use (assumes block size if of p + 1 greater each direction
        for (int i = threadIdx.x; i < mymesh->p + 1; i += blockDim.x) {
            for (int j = 0; j < mymesh->p + 1 - i; j += 1) {
                ca_element[i + (mymesh->p + 1) * j] =
                        x[mymesh->eldof[element * mymesh->dofs_per_element + mymesh->lookup_multi[i][j]]];
            }
        }

        evaluate(ca_element, mymesh->q, mymesh->p, mymesh, step_0, step_1);

        // Zero out ca_element and step_0
        for (int i = threadIdx.x;
             i < (mymesh->p + 1) * (mymesh->p + 1) + (mymesh->p + 1) * mymesh->q; i += blockDim.x) {
            s[i] = 0;
        }

        for (int i = threadIdx.x; i < mymesh->q; i += blockDim.x) {
            for (int j = 0; j < mymesh->q; ++j) {
                step_1[i + mymesh->q * j] = sin(step_1[i + mymesh->q * j]);
            }
        }

        __syncthreads();
        moment(step_1, mymesh->q, mymesh->p, mymesh, step_0, ca_element);

        for (int i = threadIdx.x; i < mymesh->p + 1; i += blockDim.x) {
            for (int j = 0; j < mymesh->p + 1 - i; j += 1) {
                ca_element[i + (mymesh->p + 1) * j] =
                        ca_element[i + (mymesh->p + 1) * j] * 2 * mymesh->areas[element];
            }
        }

        __syncthreads();
        for (int i = threadIdx.x; i < mymesh->dofs_per_element; i += blockDim.x) {
            atomicAdd_local(&(sinx[mymesh->eldof[element * mymesh->dofs_per_element + i]]),
                            ca_element[mymesh->lookup_dof[i][0] +
                                       (mymesh->p + 1) * mymesh->lookup_dof[i][1]]);
        }
    }

//    if (threadIdx.x == 0 and threadIdx.y == 0 and blockIdx.x == 0) {
//        printf("Done with matrix mult\n");
//    }

}
