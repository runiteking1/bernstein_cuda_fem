//
// Created by marshall on 5/2/19.
//

#include "edge.h"
#include <cublas_v2.h>
#include "cudaHeaders.h"
#include "mesh_reader.h"
#include "helper.cuh"

using namespace std;

double binom_cpu(const int n, const int m) {
    // Binomial coefficient from https://github.com/waura/nmc/blob/be2204213f47511132851b18c0c61a26ea1c3bba/include/nmc/FMCSM_CUDA_Laplace2D_Kernel.h
    if (n < 0) printf("error: binomial_coefficient\n");
    if (m < 0) printf("error: binomial_coefficient\n");
    if (n == m) return 1.0;
    if (m == 0) return 1.0;

    int i;
    double nn = 1.0, nm = 1.0, mm = 1.0;
    for (i = 1; i <= n; i++) {
        nn *= i;
    }
    for (i = 1; i <= (n - m); i++) {
        nm *= i;
    }
    for (i = 1; i <= m; i++) {
        mm *= i;
    }

    return nn / (nm * mm);
}

double inline mu(const int i, const int p) {
    return 64.0 * (i + 1) * (i + 2) / ((2 * i + 5) * (i + 3) * (i + 4) * (p + i + 4) * (p - i - 1));
}

__device__ void cinv(const double *f, double *work, double* temp, double **coefs, double *output, int len_f) {
    int n = len_f + 1;
    for (int i = threadIdx.x; i < n - 1; i += blockDim.x) {
        work[i] = f[i];
        output[i] = 0;
    }

    __syncthreads();

    for (int outer_counter = 0; outer_counter < n - 1; ++outer_counter) {
        int i = n - 2 - outer_counter;

        for (int counter = threadIdx.x; counter < i + 2; counter += blockDim.x) {
            temp[counter] = work[counter];
        }
        // if(threadIdx.x == 0) temp[i+1] = 0.0;
        __syncthreads();
        if (i != n - 2) {
            const int local_n = i + 3;
            for (int inner_counter = threadIdx.x; inner_counter < i + 1; inner_counter += blockDim.x) {
                work[inner_counter] = temp[inner_counter] * (local_n - 1.0 - inner_counter) / local_n +
                    temp[inner_counter + 1] * (inner_counter + 2) / local_n;
            }
        }
        __syncthreads();
        for (int j = 0; j < i + 1; j += 1) {
            if (threadIdx.x == 0) output[i] += 4 * coefs[i][j] * work[j];
        }
    }


}

__device__ void algo(const double *w, double *work, double* temp, double **coefs, double *output, int len_f) {
    double holder, holder_1;
    int p = len_f + 1;
    for (int n = 0; n < len_f; ++n) {
        // First copy work into temp 
        for (int i = threadIdx.x; i < n + 1; i += blockDim.x) {
            temp[i] = work[i];
        }

        if (n != 0) {
            if (threadIdx.x == 0) {
                work[0] = temp[0];
            }

            for (int inner_loop = threadIdx.x + 1; inner_loop < n + 2; inner_loop += blockDim.x) {
                work[inner_loop] = ((n - inner_loop) * temp[inner_loop]) / n + inner_loop * temp[inner_loop - 1] / n;
            }
        }
        __syncthreads();

        for (int i = threadIdx.x; i < n + 1; i += blockDim.x) {
            work[i] += coefs[n][i] * w[n];
        }
    }

    __syncthreads();
    for (int n = threadIdx.x; n < p - 1; n += blockDim.x) {
        output[n] = (n + 1) * work[n] / (p - 1);
        output[n] *= 4.0 * (p - 1 - n) / p;
    }
}

void prepare_preconds(mesh *mymesh, edge_info *e_info) {
    cudaMallocManaged(&(e_info->edge_diags), sizeof(double) * (mymesh->num_edges * (mymesh->p - 1)));
    cudaMallocManaged(&(e_info->vertex_diags), sizeof(double) * (mymesh->num_vertices));
    e_info->algo_coefs = mem_alloc2D_double(mymesh->p - 1, mymesh->p - 1);
    e_info->cinv_coefs = mem_alloc2D_double(mymesh->p - 1, mymesh->p - 1);

    e_info->edge_dofs = mymesh->p - 1;
    e_info->edges = mymesh->num_edges;

    for (int element = 0; element < mymesh->num_elements; ++element) {
        // Sum the vertices
        e_info->vertex_diags[mymesh->elnode[3 * element]] += 14.0 * mymesh->areas[element] / 2;
        e_info->vertex_diags[mymesh->elnode[3 * element + 1]] += 14 * mymesh->areas[element] / 2;
        e_info->vertex_diags[mymesh->elnode[3 * element + 2]] += 14 * mymesh->areas[element] / 2;

        // Create and place the indices for the edges
        for (int i = 0; i < mymesh->p - 1; ++i) {
            e_info->edge_diags[mymesh->eldof[element * mymesh->dofs_per_element + 3 + i] - mymesh->num_vertices] +=
                    mu(i, mymesh->p) * mymesh->areas[element] / 2;
            e_info->edge_diags[mymesh->eldof[element * mymesh->dofs_per_element + 3 + mymesh->p - 1 + i] -
                               mymesh->num_vertices] +=
                    mu(i, mymesh->p) * mymesh->areas[element] / 2;
            e_info->edge_diags[mymesh->eldof[element * mymesh->dofs_per_element + 3 + 2 * (mymesh->p - 1) + i] -
                               mymesh->num_vertices] +=
                    mu(i, mymesh->p) * mymesh->areas[element] / 2;

        }
    }

    for (int n = 0; n < mymesh->p - 1; ++n) {
        for (int i = 0; i < n + 1; ++i) {
            e_info->algo_coefs[n][i] =
                    pow(-1, n - i) * binom_cpu(n + 2, i + 2) * (n + 1) * (n + 2) / ((n - i + 1) * (n - i + 2));
        }
    }

    for (int i = mymesh->p - 2; i > -1; --i) {
        for (int j = 0; j < i + 1; ++j) {
            e_info->cinv_coefs[i][j] = pow(-1, i - j) * (j + 1) * binom_cpu(i + 2, j + 2) / (i - j + 2);
        }
    }
}

__global__ void process_edge(double *x, double *output, edge_info *e_info) {
    extern __shared__ double s[]; // Need a single shared extern for multiple shared memory
    double *worker = s;
    double *subworker = (double *) &worker[e_info->edge_dofs + 2];
    double *subworker2 = (double *) &subworker[e_info->edge_dofs + 2];

    for (int edge = blockIdx.x; edge < e_info->edges; edge += gridDim.x) {
        int counter = e_info->edge_dofs * edge;

        // Compute the transformation
        cinv(&(x[counter]), subworker, subworker2, e_info->cinv_coefs, worker, e_info->edge_dofs);
        __syncthreads();

        // Compute diagonals
        for (int interior = threadIdx.x; interior < e_info->edge_dofs; interior += blockDim.x) {
            worker[interior] = worker[interior] / e_info->edge_diags[counter + interior];
            if (worker[interior] != worker[interior])
            {
                printf("EMERGENCY here: %d edge: %d eddiags %g\n", interior, blockIdx.x, e_info->edge_diags[counter + interior]);
            }
            // printf("%g\n", worker[interior]);
        }
        __syncthreads();
        // Reset worker array
        for (int interior = threadIdx.x; interior < e_info->edge_dofs + 1; interior += blockDim.x) {
            subworker[interior] = 0;
        }
        __syncthreads();

        algo(worker, subworker, subworker2, e_info->algo_coefs, &(output[counter]), e_info->edge_dofs);
    }

}
