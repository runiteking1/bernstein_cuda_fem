//
// Created by marshall on 5/2/19.
//

#include "interior_inverse.h"
#include <iostream>
#include "cudaHeaders.h"

__device__ double norm_pnr222_square(const int n, const int r) {
    double h1 = 1.0 / (2.0 * r + 5.0) * ((r + 1.0) * (r + 2.0) / ((r + 3.0) * (r + 4.0)));
    double h2 = 1.0 / (2.0 * (n + 4.0)) * ((n - r + 1.0) * (n - r + 2.0) / ((n + r + 6.0) * (n + r + 7.0)));
    return h1 * h2;
}

__device__ double binom(const int n,const int m)
{
    // Binomial coefficient from https://github.com/waura/nmc/blob/be2204213f47511132851b18c0c61a26ea1c3bba/include/nmc/FMCSM_CUDA_Laplace2D_Kernel.h
//    if(n < 0) printf("error: binomial_coefficient\n");
//    if(m < 0) printf("error: binomial_coefficient\n");
    if(n == m) return 1.0;
    if(m == 0) return 1.0;

    int i;
    double nn=1.0, nm=1.0, mm=1.0;
    for(i=1; i<=n; i++){
        nn *= i;
    }
    for(i=1; i<=(n-m); i++){
        nm *= i;
    }
    for(i=1; i<=m; i++){
        mm *= i;
    }

    return nn /(nm*mm);
}

__device__ double nu22(const int nu_k, const int nu_n, const int r) {
    return ((nu_n - r + 1.0) * (nu_n - r + 2.0) / 2.0) * (pow((-1), nu_k)) * (
            binom(nu_n - r, nu_k) * binom(nu_n + r + 5, nu_k) * 2.0) / (
                           (nu_k + 1.0) * (nu_k + 2.0) * binom(nu_n, nu_k));
}


__device__ void degree_raising_1d(double *c, int len_c, double* temp) {
    int n = len_c - 1;

    // First copy into temp array
    for (int i = threadIdx.x; i < n + 1; i += blockDim.x) {
        temp[i] = c[i];
    }
    __syncthreads();

    c[0] = temp[0];
    for (int j = 1 + threadIdx.x; j < n + 1; j += blockDim.x) {
        c[j] = (n + 1 - j) * temp[j] / (n + 1.0) + j * temp[j - 1] / (n + 1.0);
    }
    c[n + 1] = temp[n];
}


__device__ void degree_raising_2d(double *c, int m, int shape, double* temp) {
    for (int k = threadIdx.x; k < m + 2; k += blockDim.x)
    {
        for (int i = 0; i < m + 2 - k; ++i)
        {
            temp[i + (shape) * k] = c[i + shape * k];
            c[i + shape * k] = 0.0;
        }
    }
    __syncthreads();
    for (int k = threadIdx.x; k < m + 2; k += blockDim.x)
    {
        for (int i = 1; i < m + 2 - k; ++i)
        {
            c[i + shape * k] += i * temp[i - 1 + shape * k] / (m + 1.0);
        }
    }
    __syncthreads();
    for (int k = threadIdx.x + 1; k < m + 2; k += blockDim.x)
    {
        for (int i = 0; i < m + 2 - k; ++i)
        {
            c[i + shape * k] += k * temp[i + shape * (k-1)] / (m + 1.0);
        }
    }
    __syncthreads();
    for (int k = threadIdx.x; k < m + 1; k += blockDim.x)
    {
        for (int i = 0; i < m + 1 - k; ++i)
        {
            c[i + shape * k] +=  (m + 1 - k - i) * temp[i + shape * k] / (m + 1.0);
        }
    }
}

__device__ double j22bcoefs(int r, int i) {
    double alpha = 2.0;
    return pow((-1.0), (r + i)) * binom(r + alpha, alpha + i) * (r + 1.0) * (r + 2) /
                          ((r + 2.0 - i) * (r + 1.0 - i));
}

__device__ void j22b2d(const double *f, const int n,
                       double *q, double *b, double *worker, double *qn, double *sn, double* temp) {
    int j;

    // Step 1 (verified)
    for (int r = 0; r < n + 1; r += 1) {
        // Equivalent to "gamma = c[r, range(0, r + 1)]"
        for (int k = threadIdx.x; k < r + 1; k += blockDim.x) {
            worker[k] = j22bcoefs(r, k);
        }

        for (int k = n - r; k > -1; --k) {
            for (int i = 0; i < n - k + 1; ++i) {
                if (threadIdx.x == 0) {
                    j = n - k - i;
                    qn[k + (n + 1) * r] = qn[k + (n + 1) * r] + worker[i] * f[i + (n + 1) * j];
                }
            }
            if (threadIdx.x == 0) {
                sn[r] = sn[r] + qn[k + (n + 1) * r] * nu22(k, n, r);
                // worker array is correct
            }
            degree_raising_1d(worker, n - k + 1, temp); 
        }
        if (threadIdx.x == 0) {
            b[n + (n + 1) * r] = sn[r] / norm_pnr222_square(n, r);
        }
    }

    __syncthreads();

    // Step 2 (verified)
    for (int i = n - 1; i > -1; --i) {
        // Reuse sn from above as Sm; need to reset to zero; only need (i+1) elements
        for (int r = threadIdx.x; r < i + 1; r += blockDim.x) {
            sn[r] = 0;
        }

        for (int r = threadIdx.x; r < i + 1; r += blockDim.x) {
            for (int k = 0; k < i - r + 1; ++k) {
                qn[k + (n + 1) * r] =
                        qn[k + 1 + (n + 1) * r] * (k + 1) / (i + 1) + qn[k + (n + 1) * r] * (i + 1 - k) / (i + 1);
                sn[r] = sn[r] + qn[k + (n + 1) * r] * nu22(k, i, r);

            }
            b[i + (n + 1) * r] = sn[r] / norm_pnr222_square(i, r);
        }
    }

    // Second stage
    for (int k = 0; k < n + 1; ++k) {
        // Reuse sn from above (previously Sm) as Tk now; reset k + 1 elements
        for (int r = threadIdx.x; r < k + 1; r += blockDim.x) {
            sn[r] = 0;
            worker[r + (n + 2) * k] = j22bcoefs(k, r);
        }

        // worker is standin for gamma_m (now needs two dimensional)
        __syncthreads();

        // Step 1 (verified)
        for (int r = 0; r < k + 1; ++r) {
            // Tk = Tk + nu22(0, m, r) * gamma_r * b[m, r]
            for (int i = threadIdx.x; i < k + 1; i += blockDim.x) {
                sn[i] = sn[i] + nu22(0, k, r) * b[k + (n + 1) * r] * worker[i + (n + 2) * r];
            }
            // Degree raise
            // if (threadIdx.x == 0) degree_raising_1d(&worker[(n + 2) * r], k + 1, temp);
            degree_raising_1d(&worker[(n + 2) * r], k + 1, temp);
        }

        __syncthreads();
        for (int i = threadIdx.x; i < k + 1; i += blockDim.x) {
            j = k - i;

            // Reusing qn from above (previously Qm, Qmp1); standin for cp
            qn[i + (n + 1) * j] = sn[i];
        }

        // Step 2; compute T_{ijk}^m recursively
        if (k > 0) {
            for (int r = 0; r < k; ++r) {
                for (int i = threadIdx.x; i < k + 2 - (r + 1); i += blockDim.x)
                {
                    temp[i] = sn[i];
                }
                __syncthreads();
                
                for (int i = threadIdx.x; i < k - (r + 1) + 1; i += blockDim.x) {
                        j = k - (r + 1) - i;
                        sn[i] = -((2 + i + 1) * temp[i + 1] / (2 + r + 1) + (2 + j + 1) * temp[i] / (2 + r + 1));
                        qn[i + (n + 1) * j] = sn[i];
                }
            }
        }
        for (int i = threadIdx.x; i < k + 1; i += blockDim.x) {
            for (int j = 0; j < i + 1; ++j) {
                    q[j + (n + 1) * (i - j)] = q[j + (n + 1) * (i - j)] + qn[j + (n + 1) * (i - j)];
            }
        }
        if (k < n) {
            // if (threadIdx.x == 0) degree_raising_2d(q, k, n + 1, temp);
            degree_raising_2d(q, k, n + 1, temp);
        }
    }
    __syncthreads();
}

__device__ void bernstein_mass_solver(const double *f, const int n, double *q, double *output, double *b,
                                      double *worker, double *qn, double *sn, double *ftilde, double* temp) {

    int ni = n - 3;
    int k, counter = 0;

//    if (threadIdx.x == 0) {
//        for (int i = 0; i < ni + 1; i += 1) {
//            for (int j = 0; j < ni - i + 1; ++j) {
//                k = ni - i - j;
//                ftilde[i + (ni + 1) * j] = (f[counter] * (i + 1) * (j + 1) * (k + 1)) / (n * (n - 1) * (n - 2));
//
////                printf("counter: %d, pos2d: %d\n", counter,  ((i - 2 * ni - 3) * (-i)) / 2 + j);
//
//                counter = counter + 1;
//            }
//        }
//    }
    for (int i = threadIdx.x; i < ni + 1; i += blockDim.x) {
        for (int j = 0; j < ni - i + 1; ++j) {
            k = ni - i - j;
            counter = ((i - 2 * ni - 3) * (-i)) / 2 + j;
            ftilde[i + (ni + 1) * j] = (f[counter] * (i + 1) * (j + 1) * (k + 1)) / (n * (n - 1) * (n - 2));
        }
    }


    __syncthreads();
    // Computation of BB-form of polynomial ptilde
    j22b2d(ftilde, ni, q, b, worker, qn, sn, temp);

    __syncthreads();

    for (int i = threadIdx.x; i < ni + 1; i += blockDim.x) {
        for (int j = 0; j < ni - i + 1; ++j) {
            k = ni - i - j;
            counter = ((i - 2 * ni - 3) * (-i)) / 2 + j;
            output[counter] = ((i + 1) * (j + 1) * (k + 1)) * q[i + (n - 2) * j] / (n * (n - 1) * (n - 2));
        }
    }

}

// This how thing is practically in serial; any way to make this faster?
__global__ void process_interior(const double *x, double *out, interior_infos *int_infos) {
    extern __shared__ double s[]; // Need a single shared extern for multiple shared memory
    double *b = s;
    double *worker = (double *) &b[(int_infos->p - 2) * (int_infos->p - 2)];
    double *qn = (double *) &worker[(int_infos->p - 1) * (int_infos->p - 1)];
    double *sn = (double *) &qn[(int_infos->p - 2) * (int_infos->p - 2)];
    double *ftilde = (double *) &sn[(int_infos->p - 2) * (int_infos->p - 2)];
    double *q = (double *) &ftilde[(int_infos->p - 2) * (int_infos->p - 2)];
    double *temp = (double*) &q[(int_infos->p - 2) * (int_infos->p - 2)];

    for (int element = blockIdx.x; element < int_infos->num_elements; element += gridDim.x) {
        int counter = int_infos->num_internal_dofs * element;
        // Zero out the shared memory
        for (int i = threadIdx.x; i < 5 * (int_infos->p - 2) * (int_infos->p - 2) +
                                      (int_infos->p - 1) * (int_infos->p - 1) + (int_infos->p - 2);
             i += blockDim.x) {
            s[i] = 0;
        }

        __syncthreads();
        bernstein_mass_solver(&(x[counter]), int_infos->p, q, &(out[counter]), b, worker, qn, sn, ftilde, temp);
        __syncthreads();

//        for (int l = 0; l < 3; ++l) {
//            printf("blockId: %d; out: %g\n", blockIdx.x, out[l]);
//        }

        // Scale each term (do not need atomics here as they are independent)
        for (int j = threadIdx.x; j < int_infos->num_internal_dofs; j += blockDim.x) {
            out[counter + j] /= 2 * int_infos->scaling[element];
//            printf("%g\n", out[counter + j]);
        }
    }
    __syncthreads();
}
