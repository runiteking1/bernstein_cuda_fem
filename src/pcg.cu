//
// Created by marshall on 5/3/19.
//

#include <cublas_v2.h>
#include "cudaHeaders.h"
#include "pcg.h"
#include "mesh_reader.h"
#include "interior_inverse.h"
#include "edge.h"
#include "preconditioner.h"
#include <stdexcept>

void set_pcg_struct(pcg_variables *pcgVariables, int dof_size) {
    cudaMalloc(&(pcgVariables->d), sizeof(double)*dof_size);
    cudaMalloc(&(pcgVariables->g), sizeof(double)*dof_size);
    cudaMalloc(&(pcgVariables->h), sizeof(double)*dof_size);
    cudaMalloc(&(pcgVariables->precond_1), sizeof(double)*dof_size);
    cudaMalloc(&(pcgVariables->precond_2), sizeof(double)*dof_size);
    cublasCreate(&(pcgVariables->handle));
}

void mass_pcg_precond(const double *b, double *x, mesh *mymesh, interior_infos *int_infos,
                      edge_info *e_info, pcg_variables *pcgVariables) {
    double delta0, delta1, tau, beta;
    int iteration = 0;

    cudaMemset(pcgVariables->g, 0, mymesh->total_dofs* sizeof(double));
    cudaMemset(pcgVariables->d, 0, mymesh->total_dofs* sizeof(double));
    double neg1 = -1.0;
    mass_multiply <<<mymesh->num_elements, 2*mymesh->q, sizeof(double) * mymesh->cuda_size_mass>>> (x, pcgVariables->g, mymesh); // g = A*x
    cublasDaxpy_v2(pcgVariables->handle, mymesh->total_dofs, &neg1, b, 1, pcgVariables->g, 1); // g = g - b

    precondition(pcgVariables->g, pcgVariables->h, pcgVariables->precond_1, pcgVariables->precond_2, mymesh, int_infos, e_info); // h = P^{-1}g
//    cublasDcopy_v2(pcgVariables->handle, mymesh->total_dofs, pcgVariables->h, 1, pcgVariables->g, 1); // k2 = k1 intiial guess


    cublasDaxpy_v2(pcgVariables->handle, mymesh->total_dofs, &neg1, pcgVariables->h, 1, pcgVariables->d, 1); // d = -h;
    // cublasDnrm2_v2(pcgVariables->handle, mymesh->total_dofs, pcgVariables->g, 1, &resid); // resid = g.norm()
//    cudaDeviceSynchronize();
//    printf("residual of initial guess: %g\n", resid);
    cublasDdot_v2(pcgVariables->handle, mymesh->total_dofs, pcgVariables->g, 1, pcgVariables->h,1, &delta0); // delta0 = g.dot(h);
    while (iteration < 80) {
        cudaMemset(pcgVariables->h, 0, mymesh->total_dofs * sizeof(double)); // h = 0
        mass_multiply <<<mymesh->num_elements, 2*mymesh->q, sizeof(double) * mymesh->cuda_size_mass>>> (pcgVariables->d, pcgVariables->h, mymesh); // h = A*d

        cublasDdot_v2(pcgVariables->handle, mymesh->total_dofs, pcgVariables->d, 1, pcgVariables->h,1, &tau); // tau = d.dot(h);
        tau = delta0 / tau; // tau = delta0/(d.dot(h))

        // g = g + tau * h;
        cublasDaxpy_v2(pcgVariables->handle, mymesh->total_dofs, &tau, pcgVariables->h, 1, pcgVariables->g, 1);

        cudaMemset(pcgVariables->h, 0, mymesh->total_dofs* sizeof(double)); // h = 0
        precondition(pcgVariables->g, pcgVariables->h, pcgVariables->precond_1, pcgVariables->precond_2, mymesh, int_infos, e_info); // h = P^{-1}g
        cublasDdot_v2(pcgVariables->handle, mymesh->total_dofs, pcgVariables->g, 1, pcgVariables->h,1, &delta1); // delta1 = g.dot(h);

        // x = x + tau * d;
        cublasDaxpy_v2(pcgVariables->handle, mymesh->total_dofs, &tau, pcgVariables->d, 1, x, 1);

        cudaDeviceSynchronize();
        if (delta1 < 1e-8) {
            break;
        }
        beta = delta1 / delta0;
        delta0 = delta1;

        cublasDscal_v2(pcgVariables->handle, mymesh->total_dofs, &beta, pcgVariables->d, 1); // d = beta*d
        cublasDaxpy_v2(pcgVariables->handle, mymesh->total_dofs, &neg1, pcgVariables->h, 1, pcgVariables->d, 1); // d = d - h;

        iteration += 1;
    }
    printf(" Iteration: %d\n", iteration);
}

//        cublasDcopy_v2(pcgVariables->handle, mymesh->total_dofs, pcgVariables->h, 1, pcgVariables->g, 1); // Copy code