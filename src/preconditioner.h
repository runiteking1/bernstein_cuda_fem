//
// Created by marshall on 5/2/19.
//

#ifndef BERNSTEIN_CUDA_PRECONDITIONER_H
#define BERNSTEIN_CUDA_PRECONDITIONER_H

#include "edge.h"
#include "mesh_reader.h"
#include "interior_inverse.h"
#include "matrix_multiply.h"

void precondition(const double *input, double *output, double *temp_rhs, double *temp_rhs2, mesh *mymesh,
                  interior_infos *int_infos, edge_info *e_info);
#endif //BERNSTEIN_CUDA_PRECONDITIONER_H
