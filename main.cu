#include <iostream>
#include "mesh_reader.h"
#include <cublas_v2.h>
#include "cudaHeaders.h"
#include "helper.cuh"
#include "jacobi_polynomial.h"
#include "matrix_multiply.h"
#include "interior_inverse.h"
#include "edge.h"
#include "preconditioner.h"
#include "pcg.h"
#include <cuda_profiler_api.h>
#include "omp.h"
#include <fstream>
#include <string>

using namespace std;


__global__ void k2_maker(const int size, const double delta_t, const double *up, const double *k1, double *u_temp) {
    for (int i = threadIdx.x + blockIdx.x * blockDim.x; i < size; i += gridDim.x * blockDim.x) {
        u_temp[i] = u_temp[i] + .5 * delta_t * up[i] + .125 * delta_t * delta_t * k1[i];
    }
}

__global__ void k3_maker(const int size, const double delta_t, const double *up, const double *k2, double *u_temp) {
    for (int i = threadIdx.x + blockIdx.x * blockDim.x; i < size; i += gridDim.x * blockDim.x) {
        u_temp[i] = u_temp[i] + delta_t * up[i] + .5 * delta_t * delta_t * k2[i];
    }
}

__global__ void update_u_up(const int size, const double delta_t, double *u, double *up, const double *k1,
    const double *k2, const double *k3) {
    for (int i = threadIdx.x + blockIdx.x * blockDim.x; i < size; i += gridDim.x * blockDim.x) {
        u[i] = u[i] + delta_t * up[i] + delta_t * delta_t * (k1[i] / 6.0 + k2[i] / 3.0);
        up[i] = up[i] + delta_t * (k1[i] / 6.0 + 2.0 * k2[i] / 3.0 + k3[i] / 6.0);
    }
}


void run_sine_gordon(int p = 4) {
    mesh *mymesh;
    cudaError_t ret = cudaMallocManaged((void**)&mymesh, sizeof(mesh));
    mymesh->p = p;
    mymesh->dofs_per_element = ((mymesh->p + 2) * (mymesh->p + 1))/2;
    printf("Order: %d\n", p);
    read_msh_file("../wave-5.mesh", mymesh, true);
    construct_elnode(mymesh, false); // Construct mesh points

    // Find the nodes and weights of the quadrature points
    int q = mymesh->p + 2; mymesh->q = q;
    mymesh->xis = mem_alloc2D_double(2, q);
    mymesh->omegas = mem_alloc2D_double(2, q);
    vector<double> weights(q), nodes(q);
    for (int i = 0; i < 2; ++i) {
        roots_sh_jacobi(q, i + 1, 1, nodes.data(), weights.data());
        for (int j = 0; j < q; ++j) {
            mymesh->xis[i][j] = nodes[j];
            mymesh->omegas[i][j] = weights[j];
        }
    }
    mymesh->cuda_size_mass = ((mymesh->p + 1) * (mymesh->p + 1) + (mymesh->p + 1) * (mymesh->q) +
      (mymesh->q * mymesh->q));
    mymesh->cuda_size_stiff = (2*(mymesh->p + 1)*(mymesh->p + 1) +
     + 2*(mymesh->p)*(mymesh->p) + 2*(mymesh->p + 1)*(mymesh->q) + 2*(mymesh->q*mymesh->q) + 18);

    // Need to create a lot of structs
    interior_infos *int_infos;
    ret = cudaMallocManaged((void **) &int_infos, sizeof(interior_infos));
    int_infos->num_internal_dofs = int((mymesh->p - 1) * (mymesh->p - 2) / 2);
    int_infos->q = mem_alloc2D_double(mymesh->p - 2, mymesh->p - 2);
    int_infos->scaling = mymesh->areas;
    int_infos->num_elements = mymesh->num_elements;
    int_infos->p = mymesh->p;
    int_infos->cuda_size = (6 * (mymesh->p - 2) * (mymesh->p - 2) + (int_infos->p - 1) * (int_infos->p - 1) +
        int_infos->p - 2) + 2*(int_infos->p*int_infos->p);

    edge_info *e_info;
    ret = cudaMallocManaged((void **) &e_info, sizeof(e_info));
    prepare_preconds(mymesh, e_info);
    e_info->cuda_size = 4 * (mymesh->p + 1)* (mymesh->p + 1);

    pcg_variables *pcgVariables;
    cudaMallocManaged((void **) &pcgVariables, sizeof(pcgVariables));
    set_pcg_struct(pcgVariables, mymesh->total_dofs);
    cudaDeviceSynchronize();

    // Now we can start doing math
    double* temp_rhs, *u, *up, *sinu, *k1, *k2, *k3, *u_temp;
    cudaMalloc(&temp_rhs,mymesh->total_dofs*sizeof(double));
    cudaMalloc(&u,       mymesh->total_dofs*sizeof(double));
    cudaMalloc(&up,      mymesh->total_dofs*sizeof(double));
    cudaMalloc(&sinu,    mymesh->total_dofs*sizeof(double));
    cudaMalloc(&k1,      mymesh->total_dofs*sizeof(double));
    cudaMalloc(&k2,      mymesh->total_dofs*sizeof(double));
    cudaMalloc(&k3,      mymesh->total_dofs*sizeof(double));
    cudaMalloc(&u_temp,  mymesh->total_dofs*sizeof(double));

    double *u_cpu;
    u_cpu = (double *) malloc(mymesh->total_dofs * sizeof(double));

    // Create load vector to set initial conditions
    cudaMemset(u, 0, mymesh->total_dofs * sizeof(double));
    load_vector<<<mymesh->num_elements, mymesh->q, sizeof(double)*((mymesh->p + 1)*(mymesh->p + 1) + (mymesh->p + 1)*(mymesh->q) + (mymesh->q*mymesh->q))>>>(temp_rhs, mymesh);
    cudaDeviceSynchronize();
    mass_pcg_precond(temp_rhs, u, mymesh, int_infos, e_info, pcgVariables);
    cudaMemset(temp_rhs, 0, mymesh->total_dofs * sizeof(double));
    mass_multiply<<<mymesh->num_elements, 2*mymesh->q, sizeof(double)*mymesh->cuda_size_mass>>>(u, temp_rhs, mymesh); // temp_rhs = Su
    cudaDeviceSynchronize();
    double dot;
    cublasDdot_v2(pcgVariables->handle, mymesh->total_dofs, u, 1, temp_rhs, 1, &dot);
    printf("Initial l2: %g\n", dot);
    cudaMemset(up, 0, mymesh->total_dofs*sizeof(double));

    const double neg1 = -1;
    const double delta_t = 0.001;
    double dot2;
    double t1, t2;
    t1 = omp_get_wtime();
    for (int t = 0; t < 1000; ++t) {
        // Solve k1
        cudaMemset(temp_rhs, 0, mymesh->total_dofs * sizeof(double));
        nonlinear_sine <<<mymesh->num_elements, mymesh->q, sizeof(double) * mymesh->cuda_size_mass>>>(u, sinu, mymesh);
        stiff_multiply<<<mymesh->num_elements, mymesh->q, sizeof(double)*mymesh->cuda_size_stiff>>>(u, temp_rhs, mymesh); // temp_rhs = Su
        cublasDscal_v2(pcgVariables->handle, mymesh->total_dofs, &neg1, temp_rhs, 1); // temp_rhs = -Su
        cublasDaxpy_v2(pcgVariables->handle, mymesh->total_dofs, &neg1, sinu, 1, temp_rhs, 1); //temp_rhs = temp_rhs - sinu
        cublasDcopy_v2(pcgVariables->handle, mymesh->total_dofs, u, 1, k1, 1); // k1 = u intiial guess
        mass_pcg_precond(temp_rhs, k1, mymesh, int_infos, e_info, pcgVariables);

        // Solve k2
        cudaMemset(temp_rhs, 0, mymesh->total_dofs * sizeof(double));
        cublasDcopy_v2(pcgVariables->handle, mymesh->total_dofs, u, 1, u_temp, 1); // u_temp = u
        k2_maker << < (mymesh->total_dofs) / 32 + 1, 32 >> >
                                                     (mymesh->total_dofs, delta_t, up, k1, u_temp); // u_temp = u + .5 * delta_t * up + .125 * delta_t * delta_t * k1
        cudaMemset(sinu, 0, mymesh->total_dofs * sizeof(double)); // sinu = 0
        nonlinear_sine << < mymesh->num_elements, mymesh->q, sizeof(double) * mymesh->cuda_size_mass >> >
                                                             (u_temp, sinu, mymesh); // sinu = sin(u_temp)

        stiff_multiply << < mymesh->num_elements, mymesh->q, sizeof(double) * mymesh->cuda_size_stiff >> >
                                                             (u_temp, temp_rhs, mymesh); // temp_rhs = Su
        cublasDscal_v2(pcgVariables->handle, mymesh->total_dofs, &neg1, temp_rhs, 1); // temp_rhs = -Su
        cublasDaxpy_v2(pcgVariables->handle, mymesh->total_dofs, &neg1, sinu, 1, temp_rhs, 1); //temp_rhs = temp_rhs - sinu
        cublasDcopy_v2(pcgVariables->handle, mymesh->total_dofs, k1, 1, k2, 1); // k2 = k1 intiial guess
        mass_pcg_precond(temp_rhs, k2, mymesh, int_infos, e_info, pcgVariables); // k2 = M^{-1}(temp_rhs)


        // Solve k3
        cudaMemset(temp_rhs, 0, mymesh->total_dofs * sizeof(double));
        cublasDcopy_v2(pcgVariables->handle, mymesh->total_dofs, u, 1, u_temp, 1); // u_temp = u
        k3_maker << < (mymesh->total_dofs) / 32 + 1, 32 >> >
                                                     (mymesh->total_dofs, delta_t, up, k2, u_temp); // u_temp = u + .5 * delta_t * up + .125 * delta_t * delta_t * k1
        cudaMemset(sinu, 0, mymesh->total_dofs * sizeof(double)); // sinu = 0
        nonlinear_sine << < mymesh->num_elements, mymesh->q, sizeof(double) * mymesh->cuda_size_mass >> >
                                                             (u_temp, sinu, mymesh); // sinu = sin(u_temp)

                                                             stiff_multiply << < mymesh->num_elements, mymesh->q, sizeof(double) * mymesh->cuda_size_stiff >> >
                                                             (u_temp, temp_rhs, mymesh); // temp_rhs = Su
        cublasDscal_v2(pcgVariables->handle, mymesh->total_dofs, &neg1, temp_rhs, 1); // temp_rhs = -Su
        cublasDaxpy_v2(pcgVariables->handle, mymesh->total_dofs, &neg1, sinu, 1, temp_rhs,
                       1); //temp_rhs = temp_rhs - sinu
        cublasDcopy_v2(pcgVariables->handle, mymesh->total_dofs, k2, 1, k3, 1); // k3 = k2 intiial guess
        mass_pcg_precond(temp_rhs, k3, mymesh, int_infos, e_info, pcgVariables); // k3 = M^{-1}(temp_rhs)

        // Update u and up
        update_u_up << < (mymesh->total_dofs) / 32 + 1, 32 >> > (mymesh->total_dofs, delta_t, u, up, k1, k2, k3);
        printf("time %d\n", t);
        if (t % 20 == 0) {
            // std::string filename = "123.txt";
            char str[20];
            sprintf(str, "hard.csv.%06d", t);
            cudaMemcpyAsync(u_cpu, u, mymesh->total_dofs * sizeof(double), cudaMemcpyDeviceToHost);
            cudaDeviceSynchronize();
            ofstream ostrm(str);
            if (ostrm.is_open()) {
                ostrm << "x coord, y coord, z coord" << endl;
                for (int count = 0; count < mymesh->num_vertices; ++count)
                {
                    ostrm << mymesh->vertices[2*count] << "," << mymesh->vertices[2*count + 1] << "," << u_cpu[count] <<endl;
                }
                ostrm.close();
            }
        }
//        cudaDeviceSynchronize();
////        printf("time: %g\n", delta_t*t);
    }
//
    t2 = omp_get_wtime();
    printf("Time: %g\n", t2 - t1);
    cudaMemset(temp_rhs, 0, mymesh->total_dofs * sizeof(double));
    mass_multiply<<<mymesh->num_elements, 2*mymesh->q, sizeof(double)*mymesh->cuda_size_mass>>>(u, temp_rhs, mymesh); // temp_rhs = Su
    cublasDdot_v2(pcgVariables->handle, mymesh->total_dofs, temp_rhs, 1, u, 1, &dot);
    // cublasDdot_v2(pcgVariables->handle, mymesh->total_dofs, up, 1, up, 1, &dot2);
    cudaDeviceSynchronize();
    printf("Final energy: %g\n", dot);
//
//    mass_multiply <<<mymesh->num_elements, mymesh->q, sizeof(double) * mymesh->cuda_size_mass>>> (sinu, up, mymesh);
//    cudaDeviceSynchronize();
//
//    dot = 0;
//    for (int k = 0; k < mymesh->total_dofs; ++k) {
//        dot += u[k]*u[k];
//    }
//    printf("%g\n", dot);

    free(u_cpu);
    cudaFree(&temp_rhs);
    cudaFree(&u);
    cudaFree(&up);
    cudaFree(&sinu);
    cudaFree(&k1);
    cudaFree(&k2);
    cudaFree(&k3);
    cudaFree(&u_temp);
    cudaFree(&(pcgVariables->d));
    cudaFree(&(pcgVariables->g));
    cudaFree(&(pcgVariables->h));
    cudaFree(&(pcgVariables->precond_1));
    cudaFree(&(pcgVariables->precond_2));

}

void profile(int p, const char*file) {
    mesh *mymesh;
    cudaError_t ret = cudaMallocManaged((void **) &mymesh, sizeof(mesh));
    mymesh->p = p;
    printf("Running profile code for order: %d\n", p);
    mymesh->dofs_per_element = ((mymesh->p + 2) * (mymesh->p + 1)) / 2;
    // read_msh_file("../wave-4.mesh", mymesh, true);
    read_msh_file(file, mymesh, true);
    construct_elnode(mymesh, false); // Construct mesh points

    // Find the nodes and weights of the quadrature points
    int q = mymesh->p + 2;
    mymesh->q = q;
    mymesh->xis = mem_alloc2D_double(2, q);
    mymesh->omegas = mem_alloc2D_double(2, q);
    vector<double> weights(q), nodes(q);
    for (int i = 0; i < 2; ++i) {
        roots_sh_jacobi(q, i + 1, 1, nodes.data(), weights.data());
        for (int j = 0; j < q; ++j) {
            mymesh->xis[i][j] = nodes[j];
            mymesh->omegas[i][j] = weights[j];
        }
    }
    mymesh->cuda_size_mass = ((mymesh->p + 1) * (mymesh->p + 1) + (mymesh->p + 1) * (mymesh->q) +
      (mymesh->q * mymesh->q));
    mymesh->cuda_size_stiff = (2 * (mymesh->p + 1) * (mymesh->p + 1) +
     +2 * (mymesh->p) * (mymesh->p) + 2 * (mymesh->p + 1) * (mymesh->q) +
     2 * (mymesh->q * mymesh->q) + 18);

    // Need to create a lot of structs
    interior_infos *int_infos;
    ret = cudaMallocManaged((void **) &int_infos, sizeof(interior_infos));
    int_infos->num_internal_dofs = int((mymesh->p - 1) * (mymesh->p - 2) / 2);
    int_infos->q = mem_alloc2D_double(mymesh->p - 2, mymesh->p - 2);
    int_infos->scaling = mymesh->areas;
    int_infos->num_elements = mymesh->num_elements;
    int_infos->p = mymesh->p;
    int_infos->cuda_size = (6 * (mymesh->p - 2) * (mymesh->p - 2) + (int_infos->p - 1) * (int_infos->p - 1) +
        int_infos->p - 2) + 2*(int_infos->p*int_infos->p);

    edge_info *e_info;
    ret = cudaMallocManaged((void **) &e_info, sizeof(e_info));
    prepare_preconds(mymesh, e_info);
    e_info->cuda_size = 2 * (mymesh->p + 1) * (mymesh->p + 1);

    // pcg_variables *pcgVariables;

    // cudaMallocManaged((void **) &pcgVariables, sizeof(pcgVariables));
    // set_pcg_struct(pcgVariables, mymesh->total_dofs);
    cudaDeviceSynchronize();

    double *Ax, *x;
    cudaMalloc(&x, mymesh->total_dofs * sizeof(double));
    cudaMalloc(&Ax, mymesh->total_dofs * sizeof(double));
    cudaMemset(x, 1, mymesh->total_dofs * sizeof(double));
    cudaMemset(Ax, 1, mymesh->total_dofs * sizeof(double));

    process_interior << < mymesh->num_elements, mymesh->q,
    sizeof(double) * int_infos->cuda_size >> > (x, Ax, int_infos);
    cudaMemset(x, 1, mymesh->total_dofs * sizeof(double));
    cudaMemset(Ax, 0, mymesh->total_dofs * sizeof(double));

    process_interior << < mymesh->num_elements, mymesh->q,
    sizeof(double) * int_infos->cuda_size >> > (x, Ax, int_infos);
    cudaMemset(x, 1, mymesh->total_dofs * sizeof(double));
    cudaMemset(Ax, 0, mymesh->total_dofs * sizeof(double));

    process_interior << < mymesh->num_elements, mymesh->q,
    sizeof(double) * int_infos->cuda_size >> > (x, Ax, int_infos);
    cudaMemset(x, 1, mymesh->total_dofs * sizeof(double));
    cudaMemset(Ax, 0, mymesh->total_dofs * sizeof(double));

    process_interior << < mymesh->num_elements, mymesh->q,
    sizeof(double) * int_infos->cuda_size >> > (x, Ax, int_infos);
    cudaMemset(x, 1, mymesh->total_dofs * sizeof(double));
    cudaMemset(Ax, 0, mymesh->total_dofs * sizeof(double));

    mass_multiply <<<mymesh->num_elements, 2*mymesh->q, sizeof(double) * mymesh->cuda_size_mass>>> (x, Ax, mymesh); // h = A*d
    cudaMemset(x, 1, mymesh->total_dofs * sizeof(double));
    cudaMemset(Ax, 0, mymesh->total_dofs * sizeof(double));

    mass_multiply <<<mymesh->num_elements, 2*mymesh->q, sizeof(double) * mymesh->cuda_size_mass>>> (x, Ax, mymesh); // h = A*d
    cudaMemset(x, 1, mymesh->total_dofs * sizeof(double));
    cudaMemset(Ax, 0, mymesh->total_dofs * sizeof(double));

    mass_multiply <<<mymesh->num_elements, 2*mymesh->q, sizeof(double) * mymesh->cuda_size_mass>>> (x, Ax, mymesh); // h = A*d
    cudaMemset(x, 1, mymesh->total_dofs * sizeof(double));
    cudaMemset(Ax, 0, mymesh->total_dofs * sizeof(double));

    mass_multiply <<<mymesh->num_elements, 2*mymesh->q, sizeof(double) * mymesh->cuda_size_mass>>> (x, Ax, mymesh); // h = A*d
    cudaDeviceSynchronize();
    cudaError_t errSync = cudaGetLastError();
    cudaError_t errAsync = cudaDeviceSynchronize();
    if (errSync != cudaSuccess) {
        printf("Sync kernel error: %s\n", cudaGetErrorString(errSync));
    }
    if (errAsync != cudaSuccess) {
        printf("Async kernel error: %s\n", cudaGetErrorString(errAsync));
    }

    // for (int i = 0; i < int_infos->num_internal_dofs * int_infos->num_elements; ++i) {
    //     printf("%g\n", Ax[i]);
    // }

    cudaFree(x);
    cudaFree(Ax);

}

int main(int argc, char const *argv[])
{
    int nDevices;

    cudaGetDeviceCount(&nDevices);
    for (int i = 0; i < nDevices; i++) {
        cudaDeviceProp prop;
        cudaGetDeviceProperties(&prop, i);
        printf("Device Number: %d\n", i);
        printf("  Device name: %s\n", prop.name);
        printf("  Memory Clock Rate (KHz): %d\n",
         prop.memoryClockRate);
        printf("  Memory Bus Width (bits): %d\n",
         prop.memoryBusWidth);
        printf("  Peak Memory Bandwidth (GB/s): %f\n",
         2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
        printf("  Compute Capability: %d.%d\n\n", prop.major, prop.minor);
    }

    run_sine_gordon();
   // profile(atoi(argv[1]), argv[2]);
    return 0;
}
